; Restaurant Admin Makefile

api = 2
core = 7.x

projects[radix_admin][type] = module
projects[radix_admin][download][type] = git
projects[radix_admin][download][revision] = 692b921
projects[radix_admin][download][branch] = 7.x-1.x
projects[radix_admin][subdir] = contrib

; projects[adminify][type] = module
; projects[adminify][download][type] = git
; projects[adminify][download][revision] = 814291d
; projects[adminify][download][branch] = 7.x-1.x
; projects[adminify][subdir] = contrib
